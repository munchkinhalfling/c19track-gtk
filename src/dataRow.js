/* dataRow.js
 *
 * Copyright 2020 munchkinhalfling <munchkinhalfling@pm.me>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


const {GObject, Gtk} = imports.gi;
const {MoreInfoWnd} = imports.moreInfoWnd;
const {posNeg, roundToHundredth} = imports.util;

var DataRow = GObject.registerClass({
  GTypeName: "DataRow",
  Template: 'resource:///com/munchkinhalfling/C19Track/dataRow.ui',
  InternalChildren: ['positive', 'state', 'percPos', 'death', 'moreInfoBtn']
}, class DataRow extends Gtk.Box {
  _init(state, stateInfo, stateDaily) {
    super._init({orientation: Gtk.Orientation.HORIZONTAL});
    this.state = state;
    this.stateInfo = stateInfo;
    this.stateDaily = stateDaily;
    this.buildChildren();
  }
  buildChildren() {
    this._state.label = this.state.state;
    this._positive.label = this.state.positive + posNeg(this.stateDaily[0].positiveIncrease);
    this._percPos.label = roundToHundredth((this.state.positive / this.state.totalTestResults) * 100) + '%';
    this._death.label = this.state.death + posNeg(this.stateDaily[0].deathIncrease);
    this._moreInfoBtn.connect('clicked', () => {
      const wnd = new MoreInfoWnd(this.state, this.stateInfo, this.stateDaily);
      wnd.present();
    });
  }
})
