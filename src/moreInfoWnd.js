/* moreInfoWnd.js
 *
 * Copyright 2020 munchkinhalfling <munchkinhalfling@pm.me>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


const {GObject, Gtk, GLib} = imports.gi;
const {posNeg, roundToHundredth} = imports.util;



var MoreInfoWnd = GObject.registerClass({
  GTypeName: "MoreInfoWnd",
  Template: 'resource:///com/munchkinhalfling/C19Track/moreInfoWnd.ui',
  InternalChildren: ['closeBtn', 'titleLbl', 'positive', 'negative', 'deaths', 'hospis', 'tests', 'websiteBtn', 'active', 'recovered', 'percPos', 'percDead', 'percHosp']
}, class extends Gtk.Window {
  _init(stateCovid, stateInfo, stateDaily) {
    super._init();
    this._closeBtn.connect('clicked', () => {
      this.close();
    });
    this._titleLbl.label = stateInfo.name;
    this._positive.label = `Positive: ${stateCovid.positive}${posNeg(stateDaily[0].positiveIncrease)}`;
    this._negative.label = `Negative: ${stateCovid.negative}`;
    this._deaths.label = `${stateCovid.death}${posNeg(stateDaily[0].deathIncrease)}`;
    this._hospis.label = `${stateCovid.hospitalizedCumulative || "Unknown"}`;
    this._tests.label = `Tests: ${stateCovid.totalTestResults}`;
    this._percPos.label = `${roundToHundredth((stateCovid.positive / stateCovid.totalTestResults) * 100)}% positive`;
    this._percHosp.label = stateCovid.hospitalizedCumulative? ` ${roundToHundredth((stateCovid.hospitalizedCumulative / stateCovid.positive) * 100)}% of cases` : '';
    this._percDead.label = `${roundToHundredth((stateCovid.death / stateCovid.positive) * 100)}% of cases`;
    this._active.label = `Active Cases: ?`;
    this._recovered.label = `Recovered: ${stateDaily[0].recovered}`;
    this._websiteBtn.connect('clicked', () => {
      GLib.spawn_command_line_async(`xdg-open ${stateInfo.covid19Site}`)
    })
  }
})
