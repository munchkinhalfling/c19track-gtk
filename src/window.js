/* window.js
 *
 * Copyright 2020 munchkinhalfling
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const { GObject, Gtk, Soup } = imports.gi;
const { DataRow } = imports.dataRow;

function jsonpa(arr) {
  return JSON.parse(`{"v": ${arr}}`).v;
}

var C19trackWindow = GObject.registerClass({
    GTypeName: 'C19trackWindow',
    Template: 'resource:///com/munchkinhalfling/C19Track/window.ui',
    InternalChildren: ['refreshBtn', 'contentBox']
}, class C19trackWindow extends Gtk.ApplicationWindow {
    _init(application) {
        super._init({ application });
        this._refreshBtn.connect('clicked', this.refresh.bind(this));
    }
    refresh() {
      this._contentBox.get_children().forEach(child => {
        this._contentBox.remove(child);
      });
      const loadingLabel = new Gtk.Label({label: "Loading..."});
      this._contentBox.pack_start(loadingLabel, true, true, 0);

      const session = new Soup.Session();
      const currentMessage = new Soup.Message({method: 'GET', uri: new Soup.URI('https://covidtracking.com/api/v1/states/current.json')});
      session.send_message(currentMessage);
      const currentData = JSON.parse(`{"states": ${currentMessage.response_body.data.toString()}}`).states;
      const dailyMsg = new Soup.Message({method: 'GET', uri: new Soup.URI('https://covidtracking.com/api/v1/states/daily.json')});
      session.send_message(dailyMsg);
      const dailyData = jsonpa(dailyMsg.response_body.data.toString());
      const infoMsg = new Soup.Message({method: 'GET', uri: new Soup.URI('https://covidtracking.com/api/v1/states/info.json')});
      session.send_message(infoMsg);
      const info = jsonpa(infoMsg.response_body.data.toString());

      this._contentBox.remove(loadingLabel);
      for(let state of currentData) {
        const curInfo = info.find(item => item.state == state.state);
        const curDaily = dailyData.filter(item => item.state == state.state);
        const row = new DataRow(state, curInfo, curDaily);
        this._contentBox.pack_start(row, true, true, 0);
        row.show();
      }
    }
});

